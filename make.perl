#!/usr/bin/env -S perl -w

use v5.28;

use strict;
use warnings;
use Getopt::Long qw(GetOptions);

use feature qw(say);

use constant
    JAVA_HOME      => '/usr/lib/jvm/java-11-openjdk/';
use constant {
    JAVAC          => JAVA_HOME.'bin/javac',
    JAVA           => JAVA_HOME.'bin/java',
    JAR            => JAVA_HOME.'bin/jar',
    JAVADOC        => JAVA_HOME.'bin/javadoc',
    ONLINE_DOCS    => 'https://docs.oracle.com/en/java/javase/11/docs/api/',
    MODULES_DIR    => 'modules',
    OUTPUT_DIR     => 'build',
    ARTIFACTS_DIR  => 'artifacts',
    JAVADOC_DIR    => 'javadoc',
    MY_PREFIX      => "ru.ifmo.rain.yurchenko.",
    TEST_PREFIX    => "info.kgeorgiy.java.advanced.",
    DEPENDENCIES   => [
        "lib",
        "/home/artem/.m2/repository/org/jetbrains/annotations/16.0.2/annotations-16.0.2.jar",
        "artifacts",
        ],
};

sub get_module_path() {
    return join ':', @{ DEPENDENCIES() };
}

sub get_my_module {
    my $module_name = shift;
    return MY_PREFIX.$module_name;
}

sub get_testing_module {
    my $module_name = shift;
    return TEST_PREFIX.$module_name;
}

sub get_jar_path {
    my $module = get_my_module(shift);
    return ARTIFACTS_DIR."/$module.jar";
}

sub compile {
    my $module_name = shift;
    my $my_module = get_my_module($module_name);
    my $module_path = get_module_path();
    my $compile_cmd = JAVAC." -d ".OUTPUT_DIR." -p $module_path --module-source-path modules -m $my_module";
    say $compile_cmd;
    system $compile_cmd;
}

sub test {
    my ($to_cp, $to_test, $module_name, $test_option, $classname, $salt) = @_;
    $salt //= "";
    my $testname = lc $classname;
    my $my_module = get_my_module($module_name);
    my $testing_module = get_testing_module($module_name);
    my $testee = "$my_module.$classname";
    if ($to_test ne "") {
        $testee .= ",$to_test"
    }
    my $module_path = get_module_path();
    my $module_cp = OUTPUT_DIR."/$my_module";
    if ($to_cp ne "") {
        $module_cp .= ":" . $to_cp;
    }
    my $quoted_salt = quotemeta $salt;
    my $test_cmd = JAVA." -classpath $module_cp -p $module_path -m $testing_module $test_option $testee $quoted_salt";
    say $test_cmd;
    system $test_cmd;
}

sub jar {
    my ($module_name, $main_class_name) = @_;
    my $module = get_my_module($module_name);
    my $main_class = "$module.$main_class_name";
    my $jar = ARTIFACTS_DIR."/$module.jar";
    my $built_path = OUTPUT_DIR."/$module";
    my $jar_cmd = JAR." --create --file=$jar --main-class=$main_class -C $built_path .";
    say $jar_cmd;
    system $jar_cmd;
}

sub runjar {
    my $module_name = shift;
    my $args = join ' ', (map quotemeta, @_);
    my $module = get_my_module($module_name);
    my $jar = ARTIFACTS_DIR."/$module.jar";
    my $module_path = get_module_path();
    my $runjar_cmd = JAVA." -p $module_path -m $module $args";
    say $runjar_cmd;
    system $runjar_cmd;
}

sub javadoc {
    my $no_testing = shift;
    my $module_name = shift;
    my $module = get_my_module($module_name);
    my $testing_module = get_testing_module($module_name);
    my $testing_subpackages = "";
    if (!$no_testing) {
        $testing_subpackages = "-subpackages $testing_module/$testing_module";
    }
    my $module_path = get_module_path();
    my $javadoc_path = JAVADOC_DIR."/$module";
    my $javadoc_cmd = JAVADOC." -link ".ONLINE_DOCS." -p $module_path --module-source-path ".MODULES_DIR." -subpackages $module/$module $testing_subpackages -d $javadoc_path -private";
    say $javadoc_cmd;
    system $javadoc_cmd;
}

no warnings 'experimental';
given (shift @ARGV) {
    when (/^compile$/) {
        my $argc = scalar @ARGV;
        if ($argc != 1) {
            say "Usage: $0 compile MODULE_NAME";
            exit;
        }
        compile(@ARGV);
    }
    when (/^test$/) {
        my $to_cp = "";
        my $to_test = "";
        GetOptions(
            'to-cp=s' => \$to_cp,
            'to-test=s' => \$to_test,
            );
        my $argc = scalar @ARGV;
        if ($argc != 3 and $argc != 4) {
            say "Usage: $0 test MODULE_NAME TEST_OPTION CLASS_NAME [SALT]";
            exit;
        }
        test($to_cp, $to_test, @ARGV);
    }
    when (/^jar$/) {
        my $argc = scalar @ARGV;
        if ($argc != 2) {
            say "Usage: $0 jar MODULE_NAME MAIN_CLASS_NAME";
            exit;
        }
        jar(@ARGV);
    }
    when (/^runjar$/) {
        my $argc = scalar @ARGV;
        if ($argc < 1) {
            say "USAGE: $0 runjar MODULE_NAME {args}";
            exit;
        }
        runjar(@ARGV);
    }
    when (/^javadoc$/) {
        my $no_testing;
        GetOptions('no-testing' => \$no_testing);
        my $argc = scalar @ARGV;
        javadoc($no_testing, @ARGV)
    }
    default {
        say "Usage: $0 [compile|test|jar|runjar] {args}";
        exit;
    }
}
