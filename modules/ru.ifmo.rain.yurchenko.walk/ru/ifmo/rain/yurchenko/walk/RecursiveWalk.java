package ru.ifmo.rain.yurchenko.walk;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class RecursiveWalk {
    public static void main(String[] args) {
        if (args == null) {
            System.out.println("Security breach!! Why do you pass null args??");
            return;
        }
        if (args.length != 2) {
            System.out.println("Usage: <input file> <output file>");
            return;
        }
        if (args[0] == null || args[1] == null) {
            System.out.println("Security breach!!Command line arguments are null!");
            return;
        }
        var in = tryMakePath(args[0]);
        if (in == null) {
            System.out.printf("'%s' is not a valid file name%n", args[0]);
            return;
        }
        var out = tryMakePath(args[1]);
        if (out == null) {
            System.out.printf("'%s' is not a valid file name%n", args[1]);
            return;
        }
        if (!Files.exists(in)) {
            System.out.printf("'%s' doesn't exist%n", in);
            return;
        }
        if (out.getParent() != null) {
            try {
                Files.createDirectories(out.getParent());
            } catch (IOException e) {
                System.out.printf("Failure to create file '%s'%n", args[1]);
                return;
            }
        }

        try (var outWriter = Files.newBufferedWriter(out)) {
            try (var lines = Files.lines(in)) {
                try {
                    lines.flatMap(RecursiveWalk::hashes).forEachOrdered(info -> {
                        try {
                            outWriter.write(String.format("%08x %s%n", info.hash, info.path));
                        } catch (IOException e) {
                            throw new UncheckedIOException(e);
                        }
                    });
                } catch (UncheckedIOException e) {
                    System.out.printf("Failure to write to '%s'%n", out);
                }
            } catch (IOException e) {
                System.out.printf("Failure to read from '%s'%n", in);
            }
        } catch (IOException e) {
            System.out.printf("Failure to write to '%s'%n", out);
        }
    }

    @Nullable
    private static Path tryMakePath(@NotNull String string) {
        try {
            return Paths.get(string);
        } catch (InvalidPathException e) {
            return null;
        }
    }

    private static Stream<FileInfo> hashes(@NotNull String path) {
        Path p = tryMakePath(path);
        if (p == null) {
            System.out.printf("'%s' is not a valid file name%n", path);
            return Stream.of(new FileInfo(path, 0));
        } else {
            return hashes(p);
        }
    }

    private static Stream<FileInfo> hashes(@NotNull Path path) {
        if (Files.isDirectory(path)) {
            try {
                return Files.list(path).flatMap(RecursiveWalk::hashes);
            } catch (IOException e) {
                System.out.printf("'%s' is inaccessible%n", path);
                return Stream.empty();
            }
        } else { // try to compute fnv
            var fnv = computeFNV(path);
            return Stream.of(new FileInfo(path.toString(), fnv));
        }
    }

    private static int computeFNV(@NotNull Path file) {
        try (var is = new BufferedInputStream(Files.newInputStream(file))) {
            int seed = 0x811c9dc5;
            int b;
            while ((b = is.read()) != -1) {
                seed *= 0x01000193;
                seed ^= b;
            }
            return seed;
        } catch (IOException e) {
            return 0;
        }
    }

    private static class FileInfo {
        String path;
        int hash;

        FileInfo(String path, int hash) {
            this.path = path;
            this.hash = hash;
        }
    }
}
