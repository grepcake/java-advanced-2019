package ru.ifmo.rain.yurchenko.hello;

import info.kgeorgiy.java.advanced.hello.HelloServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

public class HelloUDPServer implements HelloServer {
    private static final String USAGE = "HelloUDPServer port threads";
    private ExecutorService workPool;
    private Thread mainThread;
    private Selector selector;
    private ByteBuffer receiveBuffer;

    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("USAGE: " + USAGE);
            return;
        }
        try {
            int port = Integer.parseInt(args[0]);
            int threads = Integer.parseInt(args[1]);
            new HelloUDPServer().start(port, threads);
        } catch (NumberFormatException e) {
            System.err.printf("Invalid parameter, number expected%n%s%n", USAGE);
        }
    }

    @Override
    public void start(int port, int threads) {
        try {
            selector = Selector.open();
            DatagramChannel channel = DatagramChannel.open();
            channel.bind(new InetSocketAddress(port));
            channel.configureBlocking(false);
            channel.register(selector, SelectionKey.OP_READ);
            receiveBuffer = ByteBuffer.allocateDirect(channel.socket().getReceiveBufferSize());
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }
        workPool = Executors.newFixedThreadPool(threads);
        mainThread = new Thread(this::listen);
        mainThread.start();
    }

    private void listen() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                selector.select(key -> {
                    try {
                        var channel = (DatagramChannel) key.channel();
                        var receiveInfo = Utils.receiveQuery(channel, receiveBuffer);
                        workPool.execute(() -> processQuery(receiveInfo.data, channel, receiveInfo.sender));
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    } catch (RejectedExecutionException ignored) {
                    }
                });
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void processQuery(String query, DatagramChannel channel, SocketAddress senderAddress) {
        var reply = "Hello, " + query;
        try {
            channel.send(Utils.encodeBuffer(reply), senderAddress);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void close() {
        mainThread.interrupt();
        workPool.shutdownNow();
        selector.keys().forEach(key -> {
            try {
                key.channel().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}

