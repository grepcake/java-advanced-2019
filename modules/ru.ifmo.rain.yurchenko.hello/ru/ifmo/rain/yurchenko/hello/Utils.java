package ru.ifmo.rain.yurchenko.hello;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.StandardCharsets;

class Utils {
    private Utils() {
    }

    static ByteBuffer encodeBuffer(String string) {
        return StandardCharsets.UTF_8.encode(string);
    }

    static String decodeBuffer(ByteBuffer buffer) {
        return StandardCharsets.UTF_8.decode(buffer).toString();
    }

    static ReceiveInfo receiveQuery(DatagramChannel channel, ByteBuffer buffer) throws IOException {
        var sender = channel.receive(buffer);
        buffer.flip();
        var data = Utils.decodeBuffer(buffer);
        buffer.clear();
        return new ReceiveInfo(sender, data);
    }

    static class ReceiveInfo {
        final SocketAddress sender;
        final String data;

        private ReceiveInfo(SocketAddress sender, String data) {
            this.sender = sender;
            this.data = data;
        }
    }
}
