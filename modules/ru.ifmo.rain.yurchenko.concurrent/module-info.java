module ru.ifmo.rain.yurchenko.concurrent {
    requires annotations;
    requires info.kgeorgiy.java.advanced.concurrent;
    requires ru.ifmo.rain.yurchenko.mapper;
    requires info.kgeorgiy.java.advanced.mapper;
}
