package ru.ifmo.rain.yurchenko.concurrent;

import info.kgeorgiy.java.advanced.concurrent.ListIP;
import info.kgeorgiy.java.advanced.mapper.ParallelMapper;
import ru.ifmo.rain.yurchenko.mapper.ParallelMapperImpl;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * {@code ListIP} implementation
 */
public class IterativeParallelism implements ListIP {
    private final ParallelMapper mapper;

    /**
     * Construct {@code IterativeParallelism} with no predefined mapper.
     * <p>
     * New mapper will be created on every call.
     */
    public IterativeParallelism() {
        this(null);
    }

    /**
     * Construct {@code IterativeParallelism} with mapper, that will be used on every call
     *
     * @param mapper {@code ParallelMapper} to use
     */
    public IterativeParallelism(ParallelMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T maximum(int threads, List<? extends T> values, Comparator<? super T> comparator) throws InterruptedException {
        return transform(values, threads,
                stream -> stream.max(comparator),
                liftA2(BinaryOperator.maxBy(comparator))).orElseThrow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T minimum(int threads, List<? extends T> values, Comparator<? super T> comparator) throws InterruptedException {
        return maximum(threads, values, comparator.reversed());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> boolean all(int threads, List<? extends T> values, Predicate<? super T> predicate) throws InterruptedException {
        return transform(values, threads, stream -> stream.allMatch(predicate), Boolean::logicalAnd);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> boolean any(int threads, List<? extends T> values, Predicate<? super T> predicate) throws InterruptedException {
        return !all(threads, values, predicate.negate());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String join(int threads, List<?> values) throws InterruptedException {
        return transform(values, threads, stream -> stream.map(Object::toString), Stream::concat)
                .collect(Collectors.joining());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> List<T> filter(int threads, List<? extends T> values, Predicate<? super T> predicate) throws InterruptedException {
        return transform(values, threads, stream -> stream.filter(predicate), Stream::concat).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T, U> List<U> map(int threads, List<? extends T> values, Function<? super T, ? extends U> f) throws InterruptedException {
        return transform(values, threads, stream -> stream.map(f), Stream::concat).collect(Collectors.toList());
    }

    private <T, R> R transform(List<T> values, int threads, Function<Stream<T>, R> transformer, BinaryOperator<R> combiner) throws InterruptedException {
        if (mapper != null) {
            return transform(values, threads, transformer, combiner, mapper);
        } else {
            try (var localMapper = new ParallelMapperImpl(threads)) {
                return transform(values, threads, transformer, combiner, localMapper);
            }
        }
    }

    private static <T, R> R transform(List<T> values, int threads, Function<Stream<T>, R> transformer, BinaryOperator<R> combiner, ParallelMapper localMapper) throws InterruptedException {
        var chunks = cutUp(threads, values);
        assert chunks.size() > 0;
        var partialResults = localMapper.map(ch -> transformer.apply(ch.stream()), chunks);
        return partialResults.stream().reduce(combiner).orElseThrow();
    }

    private static <T> List<List<T>> cutUp(int pieces, List<T> list) {
        if (pieces <= 0) {
            throw new IllegalArgumentException("Can't cut on " + pieces + " pieces");
        }
        if (list.isEmpty()) {
            return Collections.singletonList(Collections.emptyList());
        }
        int size = list.size();
        pieces = Math.min(pieces, size);
        int excess = size % pieces;
        int generalPieceSize = size / pieces;
        List<List<T>> chunks = new ArrayList<>(pieces);
        assert generalPieceSize >= 1;
        int pieceStart = 0;
        for (int i = 0; i < pieces; ++i) {
            int pieceEnd = pieceStart + generalPieceSize;
            if (i < excess) {
                ++pieceEnd;
            }
            chunks.add(list.subList(pieceStart, pieceEnd));
            pieceStart = pieceEnd;
        }
        return chunks;
    }

    private static <T> BinaryOperator<Optional<T>> liftA2(BinaryOperator<T> f) {
        return (ml, mr) -> ml.flatMap(l -> mr.map(r -> f.apply(l, r)));
    }
}

