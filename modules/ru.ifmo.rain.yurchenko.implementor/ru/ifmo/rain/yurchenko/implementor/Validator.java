package ru.ifmo.rain.yurchenko.implementor;

import org.jetbrains.annotations.Nullable;

/**
 * An abstraction to construct valid value.
 * <p>
 * In case valid value cannot be constructed, a message explaining the problem is provided.
 *
 * @param <E> type to validate
 */
public interface Validator<E> {
    /**
     * Chech if constructed value is valid
     *
     * @return true if constructed value is valid and false otherwise
     */
    default boolean isValid() {
        return getValue() != null;
    }

    /**
     * Get the constructed value.
     * <p>
     * In case value is invalid, {@code null} is returned.
     *
     * @return constructed value or {@code null}
     */
    @Nullable
    E getValue();

    /**
     * Get description of the validation problem.
     * <p>
     * In case value is valid, {@code null} is returned.
     *
     * @return message or {@code null}
     */
    @Nullable
    String getMessage();
}
