package ru.ifmo.rain.yurchenko.implementor;

import info.kgeorgiy.java.advanced.implementor.Impler;
import info.kgeorgiy.java.advanced.implementor.ImplerException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;

import static ru.ifmo.rain.yurchenko.implementor.Utils.NAME_APPENDIX;

/**
 * Implementation of {@link info.kgeorgiy.java.advanced.implementor.Impler}
 */
public class Implementor implements Impler {
    /**
     * Produces code implementing class or interface specified by provided {@code aClass}.
     * <p>
     * Generated class classes name should be same as classes name of the type token with {@code Impl} suffix
     * added. Generated source code should be placed in the correct subdirectory of the specified
     * {@code root} directory and have correct file name. For example, the implementation of the
     * interface {@link java.util.List} should go to {@code $root/java/util/ListImpl.java}
     *
     * @param aClass type token to create implementation for.
     * @param path   root directory to place implementation to.
     * @throws info.kgeorgiy.java.advanced.implementor.ImplerException when implementation cannot be
     *                                                                 generated.
     */
    @Override
    public void implement(Class<?> aClass, Path path) throws ImplerException {
        var outputPath = path.resolve(getJavaFilePath(aClass));
        Utils.createParents(outputPath);
        String classImpl = generateClassImplementation(aClass);
        try {
            Files.writeString(outputPath, classImpl);
        } catch (IOException e) {
            throw new ImplerException("Couldn't write to " + outputPath, e);
        }
    }

    /**
     * Make relative path to {@code aClass} in the form of {@code package/name/separated/by/slashes/ClassName.java}
     *
     * @param aClass type token to make path to
     * @return the path
     */
    @NotNull
    protected Path getJavaFilePath(Class<?> aClass) {
        return getPackagePath(aClass).resolve(aClass.getSimpleName() + NAME_APPENDIX + ".java");
    }

    /**
     * Make path out of the package of {@code aClass} in the form of {@code package/name/separated/by/slashes}
     *
     * @param aClass type token to get package path of
     * @return the path
     */
    @NotNull
    protected Path getPackagePath(Class<?> aClass) {
        return Path.of(aClass.getPackageName().replace('.', '/'));
    }

    /**
     * Generate {@code String} with implementation for {@code aClass}.
     *
     * @param aClass type token to create implementation for
     * @return the string
     * @throws ImplerException when implementation cannot be generated
     */
    @NotNull
    protected String generateClassImplementation(Class<?> aClass) throws ImplerException {
        var classWriter = new StringWriter();
        try {
            new ClassBuilder(aClass, classWriter).build();
        } catch (IOException e) {
            throw new AssertionError("IO exception in StringWriter");
        }
        return classWriter.toString();
    }
}
