package ru.ifmo.rain.yurchenko.implementor;

import info.kgeorgiy.java.advanced.implementor.ImplerException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

import static ru.ifmo.rain.yurchenko.implementor.Utils.NAME_APPENDIX;

/**
 * A tool to generate given class mokup.
 */
class ClassBuilder {
    /**
     * A number of spaces per level of indentation in the generated code.
     */
    private final static int SPACES_PER_LEVEL = 4;
    /**
     * A token a mockup of which is generated
     */
    private final Class<?> token;
    /**
     * The generated mockup code goes here.
     */
    private final Writer output;

    /**
     * Offset preceding every non-empty line on the current level of indentation.
     *
     * NOTE: not stringbuilder, because write operations are usually more frequent than resizing.
     */
    private String offset = "";

    /**
     * Construct valid builder
     *
     * @param aClass type token of a class to generate mockup of
     * @param output the generated mockup code will go here
     * @throws ImplerException when mockup cannot be generated, e.g. when {@code aClass} is primitive.
     */
    ClassBuilder(@NotNull Class<?> aClass, Writer output) throws ImplerException {
        if (aClass.isPrimitive() || Modifier.isFinal(aClass.getModifiers())
                || aClass.isArray() || aClass.isEnum() || aClass == Enum.class) {
            throw new ImplerException(String.format("Can't extend '%s'", aClass.getCanonicalName()));
        }
        token = aClass;
        this.output = output;
    }

    /**
     * Initiate building of a mockup.
     *
     * @throws IOException     if an I/O error occurs in the writer.
     * @throws ImplerException when implementation cannot be generated
     */
    void build() throws IOException, ImplerException {
        applyClass();
    }

    /**
     * Build the whole class file of the mockup.
     *
     * @throws IOException     if an I/O error occurs in the writer.
     * @throws ImplerException when implementation cannot be generated
     */
    private void applyClass() throws IOException, ImplerException {
        applyPackage();
        applyBody();
    }

    /**
     * Build the package part of the mockup.
     *
     * @throws IOException if an I/O error occurs in the writer.
     */
    private void applyPackage() throws IOException {
        String packageName = token.getPackageName();
        dumpLine(String.format("package %s;%n", packageName));
    }

    /**
     * Build the mockup class declaration.
     *
     * @throws IOException     if an I/O error occurs in the writer.
     * @throws ImplerException when implementation cannot be generated
     */
    private void applyBody() throws IOException, ImplerException {
        applyHeader();
        applyBlock(this::applyConstructors, this::applyMethods);
    }

    /**
     * Build the first line of the mockup class declaration, e.g. "{@code class FooImpl extends Impl}".
     *
     * @throws IOException if an I/O error occurs in the writer.
     */
    private void applyHeader() throws IOException {
        var relation = token.isInterface() ? "implements" : "extends";
        dumpLine(String.format("class %s %s %s {", token.getSimpleName() + NAME_APPENDIX, relation, token.getSimpleName()));
    }

    /**
     * Build constructors of the mockup
     *
     * @throws IOException     if an I/O error occurs in the writer.
     * @throws ImplerException when implementation cannot be generated
     */
    private void applyConstructors() throws IOException, ImplerException {
        var ctors = token.getDeclaredConstructors();
        boolean allPrivate = Arrays.stream(ctors).allMatch(ctor -> Modifier.isPrivate(ctor.getModifiers()));
        if (!token.isInterface() && allPrivate) {
            throw new ImplerException("Utility class");
        }
        for (var ctor : ctors) {
            applyConstructor(ctor);
        }
    }

    /**
     * Build specified constructor of the mockup.
     *
     * @param constructor a constructor to build.
     * @throws IOException     if an I/O error occurs in the writer.
     * @throws ImplerException when implementation cannot be generated
     */
    private void applyConstructor(Constructor<?> constructor) throws IOException, ImplerException {
        if (Modifier.isFinal(constructor.getModifiers())) {
            return;
        }
        var builder = new ConstructorBuilder<>(constructor);
        dumpLine(builder.buildSignature() + " {");
        applyBlock(() -> dumpLine(builder.buildBodyStatement()));
    }

    /**
     * Build methods of the mockup
     *
     * @throws IOException     if an I/O error occurs in the writer.
     * @throws ImplerException when implementation cannot be generated
     */
    private void applyMethods() throws IOException, ImplerException {
        var methods = Utils.getAllAbstractMethods(token);
        for (var method : methods) {
            applyMethod(method);
        }
    }

    /**
     * Build specified method of the mockup.
     *
     * @param method a method to build.
     * @throws IOException     if an I/O error occurs in the writer.
     * @throws ImplerException when implementation cannot be generated
     */
    private void applyMethod(Method method) throws IOException, ImplerException {
        if (!Modifier.isAbstract(method.getModifiers())) {
            return;
        }
        var builder = new MethodBuilder(method);
        dumpLine("@Override");
        dumpLine(builder.buildSignature() + " {");
        applyBlock(() -> dumpLine(builder.buildBodyStatement()));
    }

    /**
     * Write specified line to the writer.
     *
     * @param line a line to write
     * @throws IOException if an I/O error occurs
     */
    private void dumpLine(String line) throws IOException {
        if (!line.isEmpty()) {
            output.write(offset);
        }
        output.write(line);
        newline();
    }

    /**
     * Write new line according to the operating system.
     *
     * @throws IOException if an I/O error occurs
     */
    private void newline() throws IOException {
        output.write(System.lineSeparator());
    }

    /**
     * Increase level of indentation by one.
     */
    private void incOffset() {
        offset += " ".repeat(SPACES_PER_LEVEL);
    }

    /**
     * Decrease level of indentation by one.
     */
    private void decOffset() {
        assert offset.length() >= SPACES_PER_LEVEL : "Can't decrease offset below zero";
        offset = offset.substring(0, offset.length() - SPACES_PER_LEVEL);
    }

    /**
     * Build block of actions, on the new level of indentation ending with the closing brace and newline.
     *
     * @param actions actions to put into the block
     * @throws IOException     if an I/O error occurs in the writer.
     * @throws ImplerException when implementation cannot be generated
     */
    private void applyBlock(StatementSupplier... actions) throws IOException, ImplerException {
        incOffset();
        for (var s : actions) {
            s.execute();
        }
        decOffset();
        dumpLine("}");
        newline();
    }

    /**
     * A functional interface for a single building action.
     */
    @FunctionalInterface
    private interface StatementSupplier {
        /**
         * Call the action.
         *
         * @throws IOException     if an I/O error occurs in the writer.
         * @throws ImplerException when implementation cannot be generated
         */
        void execute() throws IOException, ImplerException;
    }
}
