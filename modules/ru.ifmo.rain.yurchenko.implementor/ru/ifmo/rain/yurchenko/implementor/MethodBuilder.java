package ru.ifmo.rain.yurchenko.implementor;

import java.lang.reflect.Method;

/**
 * Specialization of {@link ExecutableBuilder} to build {@link Method}.
 */
class MethodBuilder extends ExecutableBuilder<Method> {
    /**
     * Construct builder of the specified method.
     *
     * @param method method to build, must be not final
     */
    MethodBuilder(Method method) {
        super(method);
    }

    /**
     * {@inheritDoc}
     *
     * Return default value of the return type.
     */
    @Override
    protected void fillBody() {
        body.append("return ");
        var rtype = executable.getReturnType();
        if (rtype == void.class) {
            body.setLength(body.length() - 1);
        } else if (rtype == double.class || rtype == float.class || rtype == long.class
                || rtype == int.class || rtype == short.class || rtype == byte.class) {
            body.append("0");
        } else if (rtype == char.class) {
            body.append("'\\0'");
        } else if (rtype == boolean.class) {
            body.append("false");
        } else {
            body.append("null");
        }
        body.append(";");
    }

    /**
     * Build method return type.
     */
    @Override
    protected void applyReturnType() {
        signature.append(executable.getGenericReturnType().getTypeName()).append(" ");
    }

    /**
     * Build method name.
     */
    @Override
    protected void applyName() {
        signature.append(executable.getName());
    }
}
