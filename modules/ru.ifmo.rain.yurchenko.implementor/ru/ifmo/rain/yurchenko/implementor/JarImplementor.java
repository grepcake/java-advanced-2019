package ru.ifmo.rain.yurchenko.implementor;

import info.kgeorgiy.java.advanced.implementor.ImplerException;
import info.kgeorgiy.java.advanced.implementor.JarImpler;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import static ru.ifmo.rain.yurchenko.implementor.Utils.remove;

/**
 * Implementation of {@link info.kgeorgiy.java.advanced.implementor.JarImpler}
 */
public class JarImplementor extends Implementor implements JarImpler {
    /**
     * Single write operation buffer size.
     */
    private static final int BUF_SIZE = 4096;

    /**
     * System Java compiler.
     */
    private static final JavaCompiler COMPILER = ToolProvider.getSystemJavaCompiler();
    /**
     * Standard file manager.
     */
    private static final StandardJavaFileManager FM = COMPILER.getStandardFileManager(null, null, StandardCharsets.UTF_8);

    /**
     * {@inheritDoc}
     */
    @Override
    public void implementJar(Class<?> token, Path jarFile) throws ImplerException {
        Path tmp = null, sourceDir, buildDir;
        try {
            try {
                tmp = Files.createTempDirectory(null);
                sourceDir = tmp.resolve("src");
                buildDir = tmp.resolve("build");
                Files.createDirectory(sourceDir);
                Files.createDirectory(buildDir);
            } catch (IOException e) {
                throw new ImplerException("Couldn't create temp directory", e);
            }

            implement(token, sourceDir);
            compileFile(sourceDir.resolve(getJavaFilePath(token)), buildDir, getClassPath(token));

            var man = new Manifest();
            man.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
            Utils.createParents(jarFile);
            var packagePath = getPackagePath(token);
            try (var target = new JarOutputStream(Files.newOutputStream(jarFile), man)) {
                addSources(buildDir, target);
            } catch (IOException e) {
                throw new ImplerException("Couldn't write to " + jarFile, e);
            }
        } finally {
            if (tmp != null) {
                try {
                    remove(tmp);
                } catch (IOException e) {
                    throw new ImplerException("Can't clean up temp directory");
                }
            }
        }
    }

    /**
     * Compile the file given as {@code sourceFile} to the {@code output} directory with the specified classpath.
     *
     * @param sourceFile file to compile
     * @param output     directory to place binary to
     * @param classPath  classpath to use when compiling
     */
    private void compileFile(Path sourceFile, Path output, String classPath) {
        assert Files.isRegularFile(sourceFile);
        var compilationUnits = FM.getJavaFileObjects(sourceFile);
        var options = List.of("-d", output.toAbsolutePath().toString(), "-cp", classPath);
        COMPILER.getTask(null, FM, null, options, null, compilationUnits).call();
    }

    /**
     * Add sources to the jar file.
     *
     * @param source directory to look for sources or a source file
     * @param target jar file to write to
     * @throws IOException if I/O error occurs
     */
    private void addSources(Path source, JarOutputStream target) throws IOException {
        addSources(source, source, target);
    }

    /**
     * Add sources to the jar file.
     *
     * @param source directory to look for sources or a source file
     * @param root   root of the sources catalog
     * @param target jar file to write to
     * @throws IOException if I/O error occurs
     */
    private void addSources(Path source, Path root, JarOutputStream target) throws IOException {
        if (Files.isDirectory(source)) {
            try (var dirStream = Files.newDirectoryStream(source)) {
                for (var nested : dirStream) {
                    addSources(nested, root, target);
                }
            }
        } else {
            var entry = createEntry(source, root);
            target.putNextEntry(entry);
            try (var in = new BufferedInputStream(Files.newInputStream(source))) {
                byte[] buf = new byte[BUF_SIZE];
                int count;
                while ((count = in.read(buf)) != -1) {
                    target.write(buf, 0, count);
                }
            }
            target.closeEntry();
        }
    }

    /**
     * Create jar entry with the specified source path
     *
     * @param source source path to create jar entry with
     * @param root   root of the sources catalog
     * @return created jar entry
     * @throws IOException if I/O error occurs
     */
    private JarEntry createEntry(Path source, Path root) throws IOException {
        var name = root.relativize(source).toString().replace("\\", "/");
        if (Files.isDirectory(source) && !name.isEmpty() && !name.endsWith("/")) {
            name += "/";
        }
        var entry = new JarEntry(name);
        entry.setTime(Files.getLastModifiedTime(source).toMillis());
        return entry;
    }

    /**
     * Get class path necessary to compile mockup of {@code aClass}
     *
     * @param aClass class to compile mockup of
     * @return the classpath
     * @throws ImplerException when implementation cannot be generated.
     */
    private String getClassPath(Class<?> aClass) throws ImplerException {
        try {
            return Path.of(aClass.getProtectionDomain().getCodeSource().getLocation().toURI()).toString();
        } catch (URISyntaxException e) {
            throw new AssertionError("location is not URI", e);
        } catch (SecurityException e) {
            throw new ImplerException("can't get classpath", e);
        }
    }
}
