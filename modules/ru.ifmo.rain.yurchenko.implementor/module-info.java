/**
 * Utility to create mockups of classes.
 *
 * @author Artem Yurchenko
 */
module ru.ifmo.rain.yurchenko.implementor {
    requires annotations;
    requires java.compiler;

    requires info.kgeorgiy.java.advanced.implementor;
}
