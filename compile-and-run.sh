#!/usr/bin/env sh

/usr/lib/jvm/java-11-openjdk/bin/javac -d build-test -p lib:/home/artem/.m2/repository/org/jetbrains/annotations/16.0.2/annotations-16.0.2.jar --module-source-path modules -m ru.ifmo.rain.yurchenko.walk
/usr/lib/jvm/java-11-openjdk/bin/java -cp build-test/ru.ifmo.rain.yurchenko.walk -p artifacts:lib:/home/artem/.m2/repository/org/jetbrains/annotations/16.0.2/annotations-16.0.2.jar -m info.kgeorgiy.java.advanced.walk RecursiveWalk ru.ifmo.rain.yurchenko.walk.RecursiveWalk
